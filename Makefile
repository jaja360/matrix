CXX = g++
CXXFLAGS = -std=c++17 -Wall -Wextra

all: test

tests/test_matrix: tests/test_matrix.cpp matrix.h
	$(CXX) $(CXXFLAGS) $< -o $@

.PHONY: test clean

test: tests/test_matrix
	./tests/test_matrix

clean:
	rm -f tests/test_matrix tests/*output*.txt

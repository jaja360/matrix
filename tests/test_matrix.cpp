#define CATCH_CONFIG_FAST_COMPILE
#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include <iostream>
#include <vector>
#include "../matrix.h"

using namespace std;

TEST_CASE("Creation, access and comparison") {
  double content[] = {1, 2, 3, 4};
  Matrix<double> m1(2, 2, content); // Constructor using static array

  vector<double> vec = {1, 2, 3, 4};
  Matrix<double> m2(2, 2, vec); // Constructor using vector

  Matrix<double> m3 = {{1, 2}, {3, 4}}; // Constructor using initializer-list

  Matrix<double> m4("./tests/data.txt"); // Constructor using a file

  // Manual construction of the matrix using operator()
  Matrix<double> m5(2); // 2x2 Matrix<double>
  m5(1,1) = 1;
  m5(1,2) = 2;
  m5(2,1) = 3;
  m5(2,2) = 4;

  SECTION("Size") {
    REQUIRE(m1.getNbLines() == 2);
    REQUIRE(m1.getNbCols() == 2);
  }

  SECTION("Access to a specific element") {
    REQUIRE(m1(1, 1) == 1);
    REQUIRE(m1(1, 2) == 2);
    REQUIRE(m1(2, 1) == 3);
    REQUIRE(m1(2, 2) == 4);
  }

  SECTION("Equality of matrices") {
    REQUIRE(m1 == m5);
    REQUIRE(m2 == m5);
    REQUIRE(m3 == m5);
    REQUIRE(m4 == m5);
  }

  SECTION("Inequality of matrices") {
    m1(2, 2) = 5;
    REQUIRE(m1 != m5);
  }
}

TEST_CASE("Row/Column matrix") {
  Matrix<double> row(1, 5);
  Matrix<double> col(5, 1);

  for(int i = 1; i <= 5; ++i) {
    row(i) = i;
    col(i) = i;
  }

  auto scalar = row * col;
  REQUIRE(scalar.getNbLines() == 1);
  REQUIRE(scalar.getNbCols() == 1);
  REQUIRE(scalar(1) == 55);
}

TEST_CASE("Basic matrix operations") {
  Matrix<double> m1 = {{1, 2}, {3, 4}};
  Matrix<double> m2 = {{4, 3}, {2, 1}};

  SECTION("Sum of two matrices") {
    REQUIRE(m1 + m2 == Matrix<double>({{5, 5}, {5, 5}}));
    REQUIRE(m2 + m1 == Matrix<double>({{5, 5}, {5, 5}}));
  }

  SECTION("Difference of two matrices") {
    REQUIRE(m1 - m2 == Matrix<double>({{-3, -1}, {1, 3}}));
    REQUIRE(m2 - m1 == Matrix<double>({{3, 1}, {-1, -3}}));
  }

  SECTION("Scalar-Matrix<double> sum") {
    REQUIRE(1 + m1 == Matrix<double>({{2, 3}, {4, 5}}));
    REQUIRE(1 + m2 == Matrix<double>({{5, 4}, {3, 2}}));
  }

  SECTION("Scalar-Matrix<double> difference") {
    REQUIRE(1 - m1 == Matrix<double>({{0, -1}, {-2, -3}}));
    REQUIRE(1 - m2 == Matrix<double>({{-3, -2}, {-1, 0}}));
  }

  SECTION("Product of two equal sized matrices") {
    REQUIRE(m1 * m2 == Matrix<double>({{8, 5}, {20, 13}}));
    REQUIRE(m2 * m1 == Matrix<double>({{13, 20}, {5, 8}}));
  }

  Matrix<double> m3 = {{1, 0}, {0, 1}, {1, 1}};

  SECTION("Product of two different sized matrices") {
    /* [1,0]   [1,2]   [1,2] */
    /* [0,1] * [3,4] = [3,4] */
    /* [1,1]           [4,6] */
    Matrix<double> m4 = m3 * m1;
    REQUIRE(m4.getNbLines() == 3);
    REQUIRE(m4.getNbCols() == 2);
    REQUIRE(m4 == Matrix<double>({{1, 2}, {3, 4}, {4, 6}}));
  }

  SECTION("Transposition") {
    Matrix<double> m3t = m3.t();
    REQUIRE(m3t.getNbLines() == 2);
    REQUIRE(m3t.getNbCols() == 3);
    REQUIRE(m3t == Matrix<double>({{1, 0, 1}, {0, 1, 1}}));
  }

  SECTION("Scalar multiplication") {
    REQUIRE(2 * m1 == Matrix<double>({{2, 4}, {6, 8}}));
  }

  SECTION("Negation of a matrix") {
    REQUIRE(-m1 == Matrix<double>({{-1, -2}, {-3, -4}}));
  }

  SECTION("Element-Wise product") {
    Matrix<double> m1 = {{1, 2, 3}, {4, 5, 6}};
    Matrix<double> m2 = {{1, 0, -1}, {0, 1, 0}};
    Matrix<double> res = {{1, 0, -3}, {0, 5, 0}};
    REQUIRE(res == prodElemWise(m1, m2));
  }

  SECTION("Inplace Element-Wise product") {
    Matrix<double> m1 = {{1, 2, 3}, {4, 5, 6}};
    Matrix<double> m2 = {{1, 0, -1}, {0, 1, 0}};
    Matrix<double> res = {{1, 0, -3}, {0, 5, 0}};
    m1.prodElemWise(m2);
    REQUIRE(m1 == res);
  }
}

TEST_CASE("Assignation, \"=\"") {
  Matrix<int> m1 = {{1, 2}, {3, 4}};
  Matrix<int> m2 = {{5, 6}, {7, 8}};

  SECTION("Verify the matrices are different") {
    REQUIRE(m1 != m2);
  }

  SECTION("Verify the matrices are equal after assignment") {
    m2 = m1;
    REQUIRE(m1 == m2);
  }

  SECTION("Ensure the assignment was a deep-copy") {
    m2(1, 1) = 12;
    REQUIRE(m1 != m2);
  }

  SECTION("Verify the assignation of an element over all cells") {
    m1.setElemAllCells(4);
    REQUIRE(m1(1, 1) == 4);
    REQUIRE(m1(1, 2) == 4);
    REQUIRE(m1(2, 1) == 4);
    REQUIRE(m1(2, 2) == 4);
  }
}

TEST_CASE("Output stream") {
  SECTION("2x2 matrix") {
    string filename = "./tests/test_output_1.txt";
    Matrix<double> m1 = {{1, 2}, {3, 4}};
    m1.exportToFile(filename);
    Matrix<double> m2(filename);
    REQUIRE(m1 == m2);
  }

  SECTION("4x3 matrix") {
    string filename = "./tests/test_output_2.txt";
    Matrix<double> m43_1 = {{1, 2, 3},
                            {1.1, 1.2, 1.3},
                            {5, 6, 7},
                            {8.1, 8.2, 8.3}};
    m43_1.exportToFile(filename);
    Matrix<double> m43_2(filename);
    REQUIRE(m43_1 == m43_2);
  }

  SECTION("Many matrices in same output file") {
    string filename = "./tests/test_output_3.txt";
    Matrix<double> m1 = {{1, 2}, {3, 4}};
    Matrix<double> m2 = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};

    ofstream ofs(filename);
    ofs << m1 << '\n' << m2 << flush;
    ofs.close();

    ifstream ifs(filename);
    Matrix<double> m3, m4;
    ifs >> m3 >> m4;
    REQUIRE(m1 == m3);
    REQUIRE(m2 == m4);
  }
}

TEST_CASE("Advanced matrix operations") {
  SECTION("Residual of a matrix") {
    Matrix<double> m1 = {{1, 3}, {7, 9}};
    Matrix<double> m2 = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
    REQUIRE(m2.residualMatrix(2, 2) == m1);

    Matrix<double> m34 = {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}};
    Matrix<double> m23 = {{1, 3, 4}, {9, 11, 12}};
    REQUIRE(m34.residualMatrix(2, 2) == m23);

    Matrix<double> m44 = {{1, 2, 3, 4},
                          {5, 6, 7, 8},
                          {9, 10, 11, 12},
                          {13, 14, 15, 16}};
    Matrix<double> m33 = {{1, 2, 3}, {5, 6, 7}, {9, 10, 11}};
    REQUIRE(m44.residualMatrix(4, 4) == m33);
  }

  SECTION("2x2 determinant") {
    Matrix<double> m0 = {{3}};
    REQUIRE(det(m0) == 3);

    Matrix<double> m1 = {{1, 3}, {7, 9}};
    REQUIRE(det(m1) == -12);

    Matrix<double> m22 = {{3, 0}, {1, 2}};
    REQUIRE(det(m22) == 6.0);

    m22(1, 2) = 3;
    REQUIRE(det(m22) == 3.0);

    m22(1, 1) = 12;
    REQUIRE(det(m22) == 21.0);

    m22(1, 1) = -12;
    REQUIRE(det(m22) == -27.0);

    Matrix<double> m22_2 = {{0, 0}, {0, 0}};
    REQUIRE(det(m22_2) == 0);
  }

  SECTION("3x3 determinant") {
    Matrix<double> m2(3);
    REQUIRE(det(m2) == 0);

    Matrix<double> m33_1 = {{1, 1, 3}, {4, 0, 6}, {7, 1, 9}};
    REQUIRE(det(m33_1) == 12);

    Matrix<double> m33_2 = {{2, 1, 3}, {1, 0, 2}, {2, 0, -2}};
    REQUIRE(det(m33_2) == 6.0);

    Matrix<double> m33_3 = {{3, -2, 4}, {2, -4, 5}, {1, 8, 2}};
    REQUIRE(det(m33_3) == -66);
  }

  SECTION("4x4 determinant") {
    Matrix<double> m44_1 = {{3, 0, 2, -1},
                            {1, 2, 0, -2},
                            {4, 0, 6, -3},
                            {5, 0, 2, 0}};
    REQUIRE(det(m44_1) == 20);

    Matrix<double> m44_2 = {{-1, 0, 1, 1},
                            {1, -2, 1, -1},
                            {1, 0, -1, 1},
                            {1, 0, 1, -1}};
    REQUIRE(det(m44_2) == -8);
  }

  SECTION("5x5 determinant") {
    Matrix<double> m55_1 = {{7, 2, -8, 4, 6},
                    {-3, -1, 4, -2, -3},
                    {6, 2, 2, 4, 7},
                    {1, 3, 7, 5, 1},
                    {-2, 2, 3, 4, 7}};
    REQUIRE(det(m55_1) == -1);

    Matrix<double> m55_2 = {{2, 3, -1, 3, 1},
                    {3, 1, -4, 3, -1},
                    {1, -2, 3, -4, 2},
                    {1, 2, -3, 2, -2},
                    {2, 0, 0, 4, -5}};
    REQUIRE(det(m55_2) == -580);

    Matrix<double> m55_3 = {{1, 3, 1, -1, 7},
                    {2, 2, 2, 0, 2},
                    {3, 1, 3, -8, 1},
                    {3, 2, 4, 1, 3},
                    {5, 2, 5, 2, 2}};
    REQUIRE(det(m55_3) == -224);

    double content_4[25] = {0};
    Matrix<double> m55_4(5, 5, content_4);
    REQUIRE(det(m55_4) == 0);
  }

  SECTION("6x6 determinant") {
    Matrix<double> m66_1 = {{3, 0, 2, -1, -3, 3},
                    {1, 2, 0, -2, 5, 2},
                    {4, 0, 6, -3, 1, 4},
                    {5, 0, 2, 0, 2, 7},
                    {6, 3, 0, 3, -3, -2},
                    {7, 9, 8, 4, 2, 1}};
    REQUIRE(det(m66_1) == -28220);

    Matrix<double> m66_2 = {{8, 0, 2, -1, -3, 3},
                    {1, 2, 0, -2, 5, 2},
                    {4, 7, 6, -3, 9, 4},
                    {5, -4, 2, -4, 2, 7},
                    {6, 3, -123, 3, -3, -2},
                    {7, 9, 8, 4, 2, 1}};
    REQUIRE(det(m66_2) == 162205);
  }

  SECTION("Comatrix") {
    Matrix<double> m1 = {{1, 3}, {7, 9}};
    REQUIRE(com(m1) == Matrix<double>({{9, -7}, {-3, 1}}));

    Matrix<double> m2 = {{1, 2}, {3, 4}};
    REQUIRE(com(m2) == Matrix<double>({{4, -3}, {-2, 1}}));

    Matrix<double> m3 = {{4, 3}, {2, 1}};
    REQUIRE(com(m3) == Matrix<double>({{1, -2}, {-3, 4}}));

    Matrix<double> m4 = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
    REQUIRE(com(m4) == Matrix<double>({{-3.0, 6.0, -3.0},
                                       {6.0, -12.0, 6.0},
                                       {-3.0, 6.0, -3}}));
  }

  SECTION("Inverse") {
    Matrix<double> m1 = {{4, 7}, {2, 6}};
    REQUIRE(inv(m1) == Matrix<double>({{0.6, -0.7}, {-0.2, 0.4}}));

    Matrix<double> m2 = {{1, 2}, {3, 4}};
    REQUIRE(inv(m2) == Matrix<double>({{-2, 1}, {1.5, -0.5}}));
  }

  SECTION("Map") {
    Matrix<double> m1 = {{1, 3}, {7, 9}};
    m1.map([](double d) {
      return 1 + (d * 2);
    });
    REQUIRE(m1 == Matrix<double>({{3, 7}, {15, 19}}));

    Matrix<double> m2 = {{1, 2}, {3, 4}};
    m2.map([](double d) {
      return d * d;
    });
    REQUIRE(m2 == Matrix<double>({{1, 4}, {9, 16}}));
  }
}

TEST_CASE("SubMatrix<double>") {
  Matrix<double> m33 = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
  Matrix<double> m22 = m33.sub(2, 2, 3, 3);
  REQUIRE(m22 == Matrix<double>({{5, 6}, {8, 9}}));

  Matrix<double> m36 = {{2, -1, 0, 1, 0, 0},
                        {-1, 2, -1, 0, 1, 0},
                        {0, -1, 2, 0, 0, 1}};
  Matrix<double> m33_1 = {{2, -1, 0}, {-1, 2, -1}, {0, -1, 2}};
  Matrix<double> m33_2 = Matrix<double>::createIdentity(3);
  REQUIRE(m36.sub(1,1,3,3) == m33_1);
  REQUIRE(m36.sub(1,4,3,6) == m33_2);
}

TEST_CASE("Identity matrix") {
  Matrix<double> m33 = {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}};
  REQUIRE(m33 == Matrix<double>::createIdentity(3));
}

TEST_CASE("Append") {
  Matrix<double> m = {{1, 2}, {3, 4}};
  Matrix<double> m3 = {{2, 1, 2}, {2, 3, 4}};

  SECTION("New left Column") {
    Matrix<double> m2 = m.addColumn(2);
    REQUIRE(m2 == m3);
  }

  SECTION("Remove first column") {
    REQUIRE(m3.rmFstColumn() == m);
  }

  SECTION("Right Append") {
    Matrix<double> rep = {{1, 0, 0, 1, 0, 0},
                          {0, 1, 0, 0, 1, 0},
                          {0, 0, 1, 0, 0, 1}};
    Matrix<double> id1 = Matrix<double>::createIdentity(3);
    Matrix<double> id2 = Matrix<double>::createIdentity(3);
    REQUIRE(id1.rightAppend(id2) == rep);
  }

  SECTION("Augment") {
    Matrix<double> rep = {{1, 2}, {3, 4}};
    Matrix<double> rep2 = {{1, 2, 1, 0}, {3, 4, 0, 1}};
    REQUIRE(rep.augment() == rep2);
  }
}

TEST_CASE("Elementary row operations") {
  SECTION("A <-> B") {
    Matrix<double> m22_1 = {{1, 2}, {3, 4}};
    Matrix<double> m22_2 = {{3, 4}, {1, 2}};
    m22_1.swapLines(1, 2);
    REQUIRE(m22_1 == m22_2);
  }

  SECTION("A <-- k*A") {
    Matrix<double> m22_1 = {{1, 2}, {3, 4}};
    Matrix<double> m22_2 = {{1, 2}, {-6, -8}};
    m22_1.multiplyLine(2, -2);
    REQUIRE(m22_1 == m22_2);
  }

  SECTION("B <-- B + k*A") {
    Matrix<double> m22_1 = {{1, 2}, {3, 4}};
    Matrix<double> m22_2 = {{1, 2}, {0, -2}};
    m22_1.addLineAToLineB(1, 2, -3);
    REQUIRE(m22_1 == m22_2);
  }
}

TEST_CASE("Reduced matrix forms") {
  Matrix<double> m1 = {{7, 8, 9}, {4, 5, 6}, {1, 2, 3}};
  Matrix<double> m2 = {{7, 8, 9}, {0, (6.0 / 7), (12.0 / 7)}, {0, 0, 0}};
  m1.toEchelonForm();
  REQUIRE(m1 == m2);
}

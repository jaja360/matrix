# C++ Matrix

## Description

- A simple to use C++ Matrix implementation
- Header-only library
- Templated matrix (e.g., some operations are valid for any types, not just numbers)
- Provide standard matrices operations (sum, product, determinant, inverse, etc.)

## Usage

- Simply include `matrix.h` in your project
- Most operations are accessed through c++ operators
- Other standard operations have intuitive name (e.g., det(m), tr(m), m.t(), etc.)
- A Makefile is included to compile and launch the unit tests

## Dependencies

- A compiler supporting C++17
- [Catch2](https://github.com/catchorg/Catch2) (to run the unit tests)

## Author

[Jaël Champagne Gareau](http://gdac2.uqam.ca/~jgareau/en)

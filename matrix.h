#ifndef MATRIX_H
#define MATRIX_H

#include <iostream>
#include <fstream>
#include <vector>
#include <cassert>
#include <cmath>
#include <utility>
#include <functional>

template <typename T>
constexpr bool isNumeric =
  std::conjunction_v<std::is_arithmetic<T>,
                     std::negation<std::is_same<T, char>>>;

template <typename T = double>
class Matrix {
 public:
  Matrix(size_t order = 0);
  Matrix(size_t nLines, size_t nCols);
  Matrix(size_t nLines, size_t nCols, const T tab[]);
  Matrix(size_t nLines, size_t nCols, const std::vector<T>& vec);
  Matrix(const std::string& filename);
  Matrix(std::initializer_list<std::initializer_list<T>>); // {{1,2},{3,4}}
  static Matrix createIdentity(size_t order);

  // Rule of 5
  ~Matrix();
  Matrix(const Matrix& m);
  Matrix& operator = (const Matrix& m);
  Matrix(Matrix&& m);
  Matrix& operator = (Matrix&& m);

  size_t getNbLines() const;
  size_t getNbCols() const;

  Matrix t() const; // Trace
  Matrix sub(size_t startLine, size_t startCol,
             size_t endLine, size_t endCol) const;
  Matrix residualMatrix(size_t line, size_t col) const;
  Matrix reshape(size_t new_nLines, size_t new_nCols) const;
  Matrix rightAppend(const Matrix& m) const;
  Matrix augment() const;
  void exportToFile(const std::string& filename) const;

  Matrix addColumn(size_t value = 0) const;
  Matrix rmFstColumn() const;

  void setElemAllCells(const T& elem);

  void map(std::function<T(T)> fct);

  void multiplyLine(size_t i, T k);
  void multiplyColumn(size_t j, T k);
  void swapLines(size_t i1, size_t i2);
  void addLineAToLineB(size_t A, size_t B, T k);
  void toEchelonForm();

  bool operator == (const Matrix& b) const;
  bool operator != (const Matrix& b) const;
  const T& operator()(size_t i) const;
  T& operator()(size_t i);
  const T& operator()(size_t i, size_t j) const;
  T& operator()(size_t i, size_t j);

  Matrix operator - () const;
  Matrix operator + (const Matrix& b) const;
  void operator += (const Matrix& b);
  Matrix operator - (const Matrix& b) const;
  void operator -= (const Matrix& b);
  Matrix operator * (const Matrix& b) const;
  void prodElemWise(const Matrix& b) const;

  friend Matrix prodElemWise(const Matrix& a, const Matrix& b) {
    static_assert(isNumeric<T>, "T must be numeric");
    assert(a.nLines == b.nLines && b.nCols == b.nCols
           && "The size of the two matrices must be the same");

    Matrix m(a.nLines, a.nCols);
    for(size_t i = 0; i < a.nLines * a.nCols; ++i)
      m.a[i] = a.a[i] * b.a[i];

    return m;
  }

  friend Matrix operator * (T lhs, const Matrix& rhs) {
    static_assert(isNumeric<T>, "T must be numeric");
    Matrix c(rhs.nLines, rhs.nCols);
    for(size_t i = 0; i < rhs.nLines * rhs.nCols; ++i)
      c.a[i] = lhs * rhs.a[i];
    return c;
  }

  friend void operator *= (T lhs, const Matrix& rhs) {
    static_assert(isNumeric<T>, "T must be numeric");
    for(size_t i = 0; i < rhs.nLines * rhs.nCols; ++i)
      rhs.a[i] *= lhs;
  }

  friend Matrix operator + (T lhs, const Matrix& rhs) {
    static_assert(isNumeric<T>, "T must be numeric");
    Matrix c(rhs.nLines, rhs.nCols);
    for(size_t i = 0; i < rhs.nLines * rhs.nCols; ++i)
      c.a[i] = lhs + rhs.a[i];
    return c;
  }

  friend void operator += (T lhs, const Matrix& rhs) {
    static_assert(isNumeric<T>, "T must be numeric");
    for(size_t i = 0; i < rhs.nLines * rhs.nCols; ++i)
      rhs.a[i] += lhs;
  }

  friend Matrix operator - (T lhs, const Matrix& rhs) {
    static_assert(isNumeric<T>, "T must be numeric");
    Matrix c(rhs.nLines, rhs.nCols);
    for(size_t i = 0; i < rhs.nLines * rhs.nCols; ++i)
      c.a[i] = lhs - rhs.a[i];
    return c;
  }

  friend Matrix operator -= (T lhs, const Matrix& rhs) {
    static_assert(isNumeric<T>, "T must be numeric");
    for(size_t i = 0; i < rhs.nLines * rhs.nCols; ++i)
      rhs.a[i] = lhs - rhs.a[i];
  }

  friend std::ostream& operator << (std::ostream& out, const Matrix& m) {
    out << m.nLines << ' ' << m.nCols << '\n';
    for(size_t i = 1; i <= m.nLines; ++i) {
      for(size_t j = 1; j <= m.nCols; ++j)
        out << m(i, j) << ' ';
      out << '\n';
    }
    return out << std::flush;
  }

  friend std::istream& operator >> (std::istream& in, Matrix& m) {
    in >> m.nLines >> m.nCols;
    if(m.a != nullptr)
      delete[] m.a;
    m.a = new T[m.nLines * m.nCols]();

    for(size_t i = 1; i <= m.nLines; ++i) {
      for(size_t j = 1; j <= m.nCols; ++j) {
        std::string s;
        in >> s;
        m(i, j) = stod(s);
      }
    }
    return in;
  }

  friend T tr(const Matrix& m) {
    static_assert(isNumeric<T>, "T must be numeric");
    assert(m.nLines == m.nCols && "The matrix must be square");
    T somme = 0;
    for(size_t i = 1; i <= m.nLines; ++i)
      somme += m(i, i);
    return somme;
  }

  friend T det(const Matrix& m) {
    static_assert(isNumeric<T>, "T must be numeric");
    assert(m.nLines == m.nCols && "The matrix must be square");
    if(m.nLines == 1)
      return m(1, 1);
    if(m.nLines == 2)
      return m(1, 1) * m(2, 2) - m(1, 2) * m(2, 1);
    T d = 0.0;
    for(size_t i = 1; i <= m.nLines; ++i)
      d += m(i, 1) * pow(-1, i + 1) * det(m.residualMatrix(i, 1));
    return d;
  }

  friend Matrix com(const Matrix& m) {
    static_assert(isNumeric<T>, "T must be numeric");
    assert(m.nLines == m.nCols && "The matrix must be square");
    Matrix comatrice(m.nLines, m.nCols);
    for(size_t i = 1; i <= m.nLines; ++i)
      for(size_t j = 1; j <= m.nCols; ++j)
        comatrice(i, j) = pow(-1, i + j) * det(m.residualMatrix(i, j));
    return comatrice;
  }

  friend Matrix inv(const Matrix& m) {
    static_assert(isNumeric<T>, "T must be numeric");
    return (1 / det(m)) * com(m).t();
  }

 private:
  T* a = nullptr;
  size_t nLines;
  size_t nCols;

  size_t findPivot(size_t iPivot, size_t jPivot) const;
  T productElement(const Matrix& b, size_t i, size_t j) const;
  void reset(size_t _nLines, size_t _nCols);

  template <typename Iterable>
  void populate(const Iterable& span);
  void populate(const T numbers[]);
}; // End Matrix declaration

template<typename T>
Matrix<T>::Matrix(size_t order) : Matrix(order, order) {}

template<typename T>
Matrix<T>::Matrix(size_t _nLines, size_t _nCols) : nLines(_nLines), nCols(_nCols) {
  if(nLines != 0 && nCols != 0)
    a = new T[nLines * nCols]();
}

template<typename T>
Matrix<T>::Matrix(size_t _nLines, size_t _nCols, const T tab[])
  : Matrix(_nLines, _nCols) {
  populate(tab);
}

template<typename T>
Matrix<T>::Matrix(size_t _nLines, size_t _nCols, const std::vector<T>& vec)
  : Matrix(_nLines, _nCols) {
  populate(vec);
}

template<typename T>
Matrix<T>::Matrix(const std::string& filename) {
  std::ifstream file(filename);
  assert(file.is_open() && "The file could not be opened");
  file >> (*this);
  file.close();
}

template<typename T>
Matrix<T>::Matrix(std::initializer_list<std::initializer_list<T>> l) {
  nLines = l.size();
  nCols = l.begin()[0].size();
  a = new T[nLines * nCols]();
  for(size_t i = 0; i < nLines; ++i)
    for(size_t j = 0; j < nCols; ++j)
      (*this)(i + 1, j + 1) = l.begin()[i].begin()[j];
}

template<typename T>
Matrix<T> Matrix<T>::createIdentity(size_t order) {
  Matrix m(order, order);
  for(size_t i = 1; i <= order; ++i)
    m(i, i) = 1;
  return m;
}

template<typename T>
Matrix<T>::~Matrix() {
  if(a != nullptr) {
    delete[] a;
    a = nullptr;
  }
}

template<typename T>
Matrix<T>::Matrix(const Matrix& m) : nLines(m.nLines), nCols(m.nCols) {
  if(a != nullptr)
    delete[] a;
  a = new T[nLines * nCols]();
  for(size_t i = 0; i < nLines * nCols; ++i)
    a[i] = m.a[i];
}

template<typename T>
Matrix<T>& Matrix<T>::operator = (const Matrix& m) {
  if(this != &m) {
    reset(m.nLines, m.nCols);
    for(size_t i = 0; i < (nLines * nCols); ++i)
      a[i] = m.a[i];
  }
  return *this;
}

template<typename T>
Matrix<T>::Matrix(Matrix&& m) : a{std::exchange(m.a, nullptr)},
  nLines{std::exchange(m.nLines, 0)}, nCols{std::exchange(m.nCols, 0)} {}

template<typename T>
Matrix<T>& Matrix<T>::operator = (Matrix&& m) {
  if(this != &m) {
    if(a != nullptr)
      delete[] a;
    a = std::exchange(m.a, nullptr);
    nLines = std::exchange(m.nLines, 0);
    nCols = std::exchange(m.nCols, 0);
  }
  return *this;
}

template<typename T>
size_t Matrix<T>::getNbLines() const {
  return nLines;
}

template<typename T>
size_t Matrix<T>::getNbCols() const {
  return nCols;
}

template<typename T>
Matrix<T> Matrix<T>::addColumn(size_t value) const {
  Matrix m(nLines, nCols + 1);
  for(size_t i = 1; i <= nLines; ++i)
    for(size_t j = 1; j <= nCols + 1; ++j)
      m(i, j) = j == 1 ? value : (*this)(i, j - 1);
  return m;
}

template<typename T>
Matrix<T> Matrix<T>::rmFstColumn() const {
  return sub(1, 2, nLines, nCols);
}

template<typename T>
Matrix<T> Matrix<T>::t() const {
  static_assert(isNumeric<T>, "T must be numeric");
  Matrix m(nCols, nLines);
  for(size_t i = 1; i <= nLines; ++i)
    for(size_t j = 1; j <= nCols; ++j)
      m(j, i) = (*this)(i, j);
  return m;
}

template<typename T>
Matrix<T> Matrix<T>::sub(size_t startLine, size_t startCol,
                   size_t endLine, size_t endCol) const {
  size_t curLine = 1;
  size_t curCol = 0;
  Matrix subMat(endLine - startLine + 1, endCol - startCol + 1);
  for(size_t lig = startLine; lig <= endLine; ++lig, ++curLine) {
    for(size_t col = startCol; col <= endCol; ++col)
      subMat(curLine, ++curCol) = (*this)(lig, col);
    curCol = 0;
  }
  return subMat;
}

template<typename T>
Matrix<T> Matrix<T>::rightAppend(const Matrix& m) const {
  assert(nLines == m.nLines
         && "The two matrices must have the same number of lines");
  Matrix n(nLines, nCols + m.nCols);
  for(size_t i = 1; i <= n.nLines; ++i)
    for(size_t j = 1; j <= n.nCols; ++j)
      n(i, j) = j <= nCols ? (*this)(i, j) : m(i, j - nCols);
  return n;
}

template<typename T>
Matrix<T> Matrix<T>::augment() const {
  return rightAppend(Matrix::createIdentity(nLines));
}

template<typename T>
void Matrix<T>::setElemAllCells(const T& elem) {
  for(size_t i = 1; i <= nLines; ++i)
    for(size_t j = 1; j <= nCols; ++j)
      (*this)(i, j) = elem;
}

template<typename T>
void Matrix<T>::map(std::function<T(T)> fct) {
  for(size_t i = 1; i <= nLines; ++i)
    for(size_t j = 1; j <= nCols; ++j)
      (*this)(i, j) = fct((*this)(i, j));
}

template<typename T>
void Matrix<T>::multiplyLine(size_t i, T k) {
  static_assert(isNumeric<T>, "T must be numeric");
  for(size_t j = 1; j <= nCols; ++j)
    (*this)(i, j) *= k;
}

template<typename T>
void Matrix<T>::multiplyColumn(size_t j, T k) {
  static_assert(isNumeric<T>, "T must be numeric");
  for(size_t i = 1; i <= nLines; ++i)
    (*this)(i, j) *= k;
}

template<typename T>
void Matrix<T>::swapLines(size_t i1, size_t i2) {
  if(i1 == i2)
    return;
  for(size_t j = 1; j <= nCols; ++j)
    std::swap((*this)(i1, j), (*this)(i2, j));
}

template<typename T>
void Matrix<T>::addLineAToLineB(size_t A, size_t B, T k) {
  static_assert(isNumeric<T>, "T must be numeric");
  for(size_t j = 1; j <= nCols; ++j)
    (*this)(B, j) += k * (*this)(A, j);
}

template<typename T>
void Matrix<T>::toEchelonForm() {
  static_assert(isNumeric<T>, "T must be numeric");
  size_t iPivot = 0;
  size_t jPivot = 0;
  while(++iPivot <= nLines && ++jPivot <= nCols) {
    size_t iMax = findPivot(iPivot, jPivot);
    if((*this)(iMax, jPivot) == 0) {
      ++jPivot;
      continue;
    }
    swapLines(iMax, iPivot);
    for(size_t i = iPivot + 1; i <= nLines; ++i) {
      T k = (*this)(i, jPivot) / (*this)(iPivot, jPivot);
      addLineAToLineB(iPivot, i, -k);
    }
  }
}

template<typename T>
size_t Matrix<T>::findPivot(size_t iPivot, size_t jPivot) const {
  T max = std::numeric_limits<T>::min();
  size_t iMax = 0;
  for(size_t i = iPivot; i <= nLines; ++i)
    if(std::abs((*this)(i, jPivot)) > max) {
      max = std::abs((*this)(i, jPivot));
      iMax = i;
    }
  return iMax;
}

template<typename T>
void Matrix<T>::populate(const T numbers[]) {
  for(size_t i = 0; i < nLines * nCols; ++i)
    a[i] = numbers[i];
}

template<typename T>
template<typename Iterable>
void Matrix<T>::populate(const Iterable& span) {
  size_t i = -1;
  for(auto it = begin(span); it != end(span); ++it)
    a[++i] = *it;
}

template<typename T>
const T& Matrix<T>::operator()(size_t i) const {
  assert((nLines == 1 || nCols == 1) && (i >= 1) && (i <= nLines*nCols) &&
         "The row and/or column index is not valid");
  return a[i - 1];
}

template<typename T>
T& Matrix<T>::operator()(size_t i) {
  assert((nLines == 1 || nCols == 1) && (i >= 1) && (i <= nLines*nCols) &&
         "The row and/or column index is not valid");
  return a[i - 1];
}

template<typename T>
const T& Matrix<T>::operator()(size_t i, size_t j) const {
  assert(i >= 1 && i <= nLines && j >= 1 && j <= nCols &&
         "The row and/or column index is not valid");
  return a[(i - 1) * nCols + (j - 1)];
}

template<typename T>
T& Matrix<T>::operator()(size_t i, size_t j) {
  assert(i >= 1 && i <= nLines && j >= 1 && j <= nCols &&
         "The row and/or column index is not valid");
  return a[(i - 1) * nCols + (j - 1)];
}

template<typename T>
Matrix<T> Matrix<T>::operator - () const {
  static_assert(isNumeric<T>, "T must be numeric");
  return (-1) * (*this);
}

template<typename T>
Matrix<T> Matrix<T>::operator + (const Matrix& b) const {
  static_assert(isNumeric<T>, "T must be numeric");
  assert(b.nLines == nLines && b.nCols == nCols
         && "The size of the two matrices must be the same");
  Matrix c(nLines, nCols);
  for(size_t i = 1; i <= b.nLines; ++i)
    for(size_t j = 1; j <= b.nCols; ++j)
      c(i, j) = (*this)(i, j) + b(i, j);
  return c;
}

template<typename T>
void Matrix<T>::operator += (const Matrix& b) {
  static_assert(isNumeric<T>, "T must be numeric");
  (*this) = (*this) + b;
}

template<typename T>
Matrix<T> Matrix<T>::operator - (const Matrix& b) const {
  static_assert(isNumeric<T>, "T must be numeric");
  return (*this) + (-1) * b;
}

template<typename T>
void Matrix<T>::operator -= (const Matrix& b) {
  static_assert(isNumeric<T>, "T must be numeric");
  (*this) = (*this) - b;
}

template<typename T>
bool Matrix<T>::operator == (const Matrix& b) const {
  if(nLines != b.nLines || nCols != b.nCols)
    return false;

  for(size_t i = 0; i < (nLines * nCols); ++i) {
    if constexpr (std::is_floating_point_v<T>) {
      if(std::fabs(a[i] - b.a[i]) >= 1e-9)
        return false;
    } else {
      if(a[i] != b.a[i])
        return false;
    }
  }

  return true;
}

template<typename T>
bool Matrix<T>::operator != (const Matrix& b) const {
  return !(*this == b);
}

template<typename T>
void Matrix<T>::reset(size_t _nLines, size_t _nCols) {
  if(a != nullptr)
    delete[] a;
  nLines = _nLines;
  nCols = _nCols;
  a = new T[nLines * nCols]();
}

template<typename T>
Matrix<T> Matrix<T>::operator * (const Matrix& b) const {
  static_assert(isNumeric<T>, "T must be numeric");
  assert(nCols == b.nLines
         && "#columns of first matrix must be equal to #rows of other matrix");
  Matrix c(nLines, b.nCols);
  for(size_t i = 1; i <= nLines; ++i)
    for(size_t j = 1; j <= b.nCols; ++j)
      c(i, j) = productElement(b, i, j);
  return c;
}

template<typename T>
T Matrix<T>::productElement(const Matrix& b, size_t i, size_t j) const {
  T res = 0.0;
  for(size_t k = 1; k <= nCols; ++k)
    res += (*this)(i, k) * b(k, j);
  return res;
}

template<typename T>
void Matrix<T>::prodElemWise(const Matrix& b) const {
  static_assert(isNumeric<T>, "T must be numeric");
  assert(nLines == b.nLines && nCols == b.nCols
         && "The size of the two matrices must be the same");

  for(size_t i = 0; i < nLines * nCols; ++i)
    a[i] *= b.a[i];
}

template<typename T>
Matrix<T> Matrix<T>::residualMatrix(size_t line, size_t col) const {
  Matrix m(nLines - 1, nCols - 1);
  for(size_t curLine = 1, i = 1; i <= m.nLines; ++curLine, ++i) {
    for(size_t curCol = 1, j = 1; j <= m.nCols; ++curCol, ++j) {
      if(curLine == line) ++curLine;
      if(curCol == col) ++curCol;
      m(i, j) = (*this)(curLine, curCol);
    }
  }
  return m;
}

template<typename T>
Matrix<T> Matrix<T>::reshape(size_t new_nLines, size_t new_nCols) const {
  Matrix new_mat(new_nLines, new_nCols);
  size_t min_nLines = nLines < new_nLines ? nLines : new_nLines;
  size_t min_nCols = nCols < new_nCols ? nCols : new_nCols;
  for(size_t i = 1; i <= min_nLines; ++i)
    for(size_t j = 1; j <= min_nCols; ++j)
      new_mat(i, j) = (*this)(i, j);
  return new_mat;
}

template<typename T>
void Matrix<T>::exportToFile(const std::string& filename) const {
  std::fstream file(filename, std::fstream::out);
  file << (*this);
  file.close();
}

#endif
